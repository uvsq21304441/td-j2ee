package service;

import dao.DaoArmeImpl;
import dao.DaoElfeImpl;
import entity.Elfe;

public class ServiceElfeImpl implements ServiceElfe {

	@Override
	public void save(Elfe pers) {
		DaoElfeImpl de = new DaoElfeImpl();
		DaoArmeImpl da = new DaoArmeImpl();
		de.savePersonne(pers);
		da.savePersonne(pers.getArme());	
	}

	
}
