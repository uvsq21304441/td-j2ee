package service;

import entity.Elfe;

public interface ServiceElfe extends Service<Elfe> {

	void save(Elfe pers);
}
