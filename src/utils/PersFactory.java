package utils;
import java.util.HashMap;

import entity.Arme;
import entity.Personnage;
import service.Service;

public class PersFactory {

	HashMap<String, String> map = new HashMap<String,String>();
	HashMap<String, String> mapS = new HashMap<String,String>();
	
	private static PersFactory instance = null;
	
	public static PersFactory getInstance(){
		if(instance==null)
		{
			instance = new PersFactory();
		}
		return instance;
	}
	
	private PersFactory(){
		map.put("Elfe", "entity.Elfe");
		map.put("Hobbit", "entity.Hobbit");
		map.put("Nain", "entity.Nain");
		
		mapS.put("Elfe","service.ServiceElfeImpl");
	}
	
	public Personnage CreatePers(String race){
		Personnage p = null;
		String ClassName = map.get(race);

		try{
			Class maClass = Class.forName(ClassName);
			p = (Personnage) maClass.newInstance();
		}catch(Exception e){
			e.printStackTrace();
		}
		
		return p;
	}
	
	public Arme getArme(){
		return new Arme();
	}
	
	public Service getService(String service){
		Service serv = null;
		String ClassName = mapS.get(service);
		
		try{
			Class maClass = Class.forName(ClassName);
			serv = (Service) maClass.newInstance();
		}catch (Exception e) {
			e.printStackTrace();
		}
		
		return serv;
	}
}
