package dao;

import java.util.List;

public interface Dao<T> {

	public void savePersonne(T obj);
	public void deletePersonne(T obj);
	public void update(T obj);
	
	public T findById(int id);
	public List<T> findAll();
}
