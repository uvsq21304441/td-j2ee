package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import entity.Elfe;

public class DaoElfeImpl implements DaoElfe{

	@Override
	public void savePersonne(Elfe obj) {
		Connection con = null;
        PreparedStatement pst = null;
        
        try{
        	con = ConnexionBD.getCon();
        	pst = con.prepareStatement("INSERT INTO elfe VALUES(?,?,NULL)");
        	pst.setString(1, obj.getNom());
        	pst.setString(2, obj.description());
            pst.executeUpdate();
            
        }catch(Exception e)
        {
        	e.printStackTrace();
        }
	}

	@Override
	public void deletePersonne(Elfe obj) {
		Connection con = null;
        PreparedStatement pst = null;
        
        try{
        	con = ConnexionBD.getCon();
        	pst = con.prepareStatement("DELETE FROM elfe WHERE nom LIKE ?");
        	pst.setString(1, obj.getNom());
            pst.executeUpdate();
            
        }catch(Exception e)
        {
        	e.printStackTrace();
        }
	}
	
	@Override
	public void update(Elfe obj) {
		Connection con = null;
        PreparedStatement pst = null;
        
        try{
        	con = ConnexionBD.getCon();
        	pst = con.prepareStatement("UPDATE elfe SET description=? WHERE nom LIKE ?");
        	pst.setString(1, obj.description());
        	pst.setString(2, obj.getNom());
            pst.executeUpdate();
            
        }catch(Exception e)
        {
        	e.printStackTrace();
        }
	}

	@Override
	public Elfe findById(int id) {
		Connection con = null;
        PreparedStatement pst = null;
        ResultSet rs = null;
        Elfe elfe = null;
        
        try{
        	con = ConnexionBD.getCon();
        	pst = con.prepareStatement("SELECT * FROM elfe WHERE id=?");
        	pst.setInt(1, id);
            rs = pst.executeQuery();
            
            if(rs.next()){
	            elfe = new Elfe();
	           	elfe.setRace("elfe");
	           	elfe.setNom(rs.getString("nom"));
	           	elfe.setDesc(rs.getString("description"));
            }
         
        }catch(Exception e)
        {
        	e.printStackTrace();
        }
		return elfe;
	}

	@Override
	public List<Elfe> findAll() {
		Connection con = null;
        PreparedStatement pst = null;
        ResultSet rs = null;
        Elfe elfe = null;
        List<Elfe> list = new ArrayList<Elfe>();
        
        try{
        	con = ConnexionBD.getCon();
        	pst = con.prepareStatement("SELECT * FROM elfe");
            rs = pst.executeQuery();
            
            while(rs.next())
            {
            	elfe = new Elfe();
            	elfe.setRace("elfe");
            	elfe.setNom(rs.getString("nom"));
            	elfe.setDesc(rs.getString("description"));
            	list.add(elfe);
            }
        }catch(Exception e)
        {
        	e.printStackTrace();
        }
		return list;
	}

	@Override
	public Elfe findByName(String Name) {
		Connection con = null;
        PreparedStatement pst = null;
        ResultSet rs = null;
        Elfe elfe = null;
        
        try{
        	con = ConnexionBD.getCon();
        	pst = con.prepareStatement("SELECT * FROM elfe WHERE nom LIKE ?");
        	pst.setString(1, Name);
            rs = pst.executeQuery();
            
            if(rs.next()){
	            elfe = new Elfe();
	           	elfe.setRace("elfe");
	           	elfe.setNom(rs.getString("nom"));
	           	elfe.setDesc(rs.getString("description"));
            }
         
        }catch(Exception e)
        {
        	e.printStackTrace();
        }
		return elfe;
	}

}
