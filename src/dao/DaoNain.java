package dao;

import entity.Nain;

public interface DaoNain extends Dao<Nain> {

	public Nain findByName(String Name);
}
