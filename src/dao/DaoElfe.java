package dao;

import entity.Elfe;

public interface DaoElfe extends Dao<Elfe>{

	public Elfe findByName(String Name);
}
