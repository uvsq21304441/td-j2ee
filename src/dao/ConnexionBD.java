package dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.util.ArrayList;
import java.util.List;

import entity.Elfe;

public class ConnexionBD {

	private static Connection con = null;
	private static ConnexionBD conBD = null;
	
	private ConnexionBD(){
		
	}
	
	public static Connection getCon(){
		if(conBD==null){
			conBD = new ConnexionBD();
			try{
				System.out.println("YO !");
				Class.forName("com.mysql.jdbc.Driver");
				con = DriverManager.getConnection("jdbc:mysql://localhost/communaute","root","");
				
			}catch(Exception e){
				e.printStackTrace();
			}
		}
		return con;
	}

	public static void main(String args[]){
	
		DaoElfeImpl de = new DaoElfeImpl();
		List<Elfe> l = new ArrayList<Elfe>();
		l = de.findAll();
		
		for (Elfe elfe : l) {
			System.out.println(elfe.description());
			
		}
		
		Elfe e = new Elfe();
		e = de.findByName("thranduil");
		System.out.println(e.description());
		
		Elfe e2 = new Elfe();
		e2 = de.findById(2);
		System.out.println(e2.description());
		
		Elfe e3 =new Elfe();
		e3.setNom("Legolas");
		e3.setDesc(" elfe etant das la communaute");
		//de.savePersonne(e3);
		
		e3.setDesc("Une nouvelle description");
		
		de.update(e3);
		
		//de.deletePersonne(e3);
		
	}
}
