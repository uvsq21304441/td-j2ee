package controler;


import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import entity.Arme;
import entity.Elfe;
import entity.Personnage;
import service.Service;
import service.ServiceElfeImpl;
import utils.PersFactory;

/**
 * Servlet implementation class AjoutPers
 */
@WebServlet("/AjoutPers")
public class AjoutPers extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String nom = request.getParameter("nom");
		String race = request.getParameter("race");
		String desc = request.getParameter("desc");
		
		String arme_desc = request.getParameter("categorie");
		
		PersFactory factory = PersFactory.getInstance();
		Personnage p = factory.CreatePers(race);
		System.out.println(p);
		Arme arme = factory.getArme();
		
		arme.setCategorie(arme_desc);
		
		
		p.setNom(nom);
		p.setRace(race);
		p.setDesc(desc);
		p.setArme(arme);
		
		Service serv = factory.getService(race);
		serv.save(p);
	}

}
