package entity;

public class Nain extends Personnage {

	private String description;
	
	public Nain(){
		
	}
	
	@Override
	public String description() {
		return description;
		
	}

	@Override
	public void setDesc(String desc) {
		description = getNom() + " " + getRace() + " " + desc;
		
	}

	
}
