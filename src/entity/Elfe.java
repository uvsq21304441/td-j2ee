package entity;

public class Elfe extends Personnage {

	private String description;
	
	
	public Elfe() {
	}

	@Override
	public String description() {
		return description;
		
	}

	@Override
	public void setDesc(String desc) {
		description = desc;
		
	}

}
