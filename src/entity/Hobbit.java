package entity;

public class Hobbit extends Personnage {

	private String description;
	
	public Hobbit() {
	}
	
	@Override
	public String description() {
		return description;
		
	}

	@Override
	public void setDesc(String desc) {
		description = getNom() + " " + getRace() + " " + desc;
		
	}
}
