package entity;


/**@author yannd
 * Un personnage a une unique race et ne peut en changer
 * Le personnage peut changer d'arme => Pattern strategie car l'arme est �volutif (Ex: Mode de livraison de Amazon : avion, drone, camion etc...)
 * 
 * On est en inversion de controle, alors race et arme ne peuvent pas �tre mit ds le constructeur
 */
public abstract class Personnage {
	
	private String nom;
	private String race;
	private Arme arme;
	
	public abstract void setDesc(String desc);
	
	public Personnage()
	{
		
	}
	
	public Personnage(String nom) {
		super();
		this.nom = nom;
	}
	
	public String getNom() {
		return nom;
	}
	public void setNom(String nom) {
		this.nom = nom;
	}
	public String getRace() {
		return race;
	}
	public void setRace(String race) {
		this.race = race;
	}
	public Arme getArme() {
		return arme;
	}
	public void setArme(Arme arme) {
		this.arme = arme;
	}
	
	public abstract String description();

}
